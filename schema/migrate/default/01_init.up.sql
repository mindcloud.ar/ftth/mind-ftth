CREATE TABLE `project` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `esSubProyecto` TINYINT NOT NULL DEFAULT '0',
  `idProyectoPadre` BIGINT UNSIGNED,
  `nombre` VARCHAR(45) NULL,
  `descripcion` TEXT NOT NULL,
  `latitud` DOUBLE NULL,
  `longitud` DOUBLE NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `project`
ADD INDEX `fk_proyecto_padre_idx` (`idProyectoPadre` ASC) VISIBLE;

ALTER TABLE `project`
ADD CONSTRAINT `fk_sub_proyecto`
  FOREIGN KEY (`idProyectoPadre`)
  REFERENCES `project` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
