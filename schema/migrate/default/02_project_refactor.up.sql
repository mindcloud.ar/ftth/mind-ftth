ALTER TABLE `project`
DROP FOREIGN KEY `fk_sub_proyecto`;
ALTER TABLE `project`
CHANGE COLUMN `esSubProyecto` `is_sub_project` TINYINT NOT NULL DEFAULT '0' ,
CHANGE COLUMN `idProyectoPadre` `parent_id` BIGINT UNSIGNED NULL DEFAULT NULL ,
CHANGE COLUMN `nombre` `name` VARCHAR(45) NULL DEFAULT NULL ,
CHANGE COLUMN `descripcion` `description` TEXT NULL ,
CHANGE COLUMN `latitud` `lat` DOUBLE NULL DEFAULT NULL ,
CHANGE COLUMN `longitud` `long` DOUBLE NULL DEFAULT NULL ;
ALTER TABLE `project`
ADD CONSTRAINT `fk_sub_proyecto`
  FOREIGN KEY (`parent_id`)
  REFERENCES `project` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
