CREATE TABLE `access_point_type` (
  `id` VARCHAR(50) NOT NULL,
  `name` VARCHAR(100) NULL,
  `description` VARCHAR(255) NULL,
  `icon` TEXT NULL,
  PRIMARY KEY (`id`));

INSERT INTO `access_point_type` (`id`, `name`, `description`) VALUES ('node', 'Nodo', 'Nodo Principal');
INSERT INTO `access_point_type` (`id`, `name`, `description`) VALUES ('ce', 'Caja de Empalme', '');
INSERT INTO `access_point_type` (`id`, `name`) VALUES ('cto', 'Caja de Abonados');
INSERT INTO `access_point_type` (`id`, `name`) VALUES ('customer', 'Abonado');
