CREATE TABLE `access_point` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `id_type` VARCHAR(50) NULL,
  `lat` DOUBLE NULL,
  `long` DOUBLE NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_type_idx` (`id_type` ASC) VISIBLE,
  CONSTRAINT `fk_type`
    FOREIGN KEY (`id_type`)
    REFERENCES `access_point_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
