package handlers

import (
	"errors"
	"net/http"
	"strconv"

	"gitlab.com/mindcloud.ar/dev-tools/go-tools/middleware"
)

var (
	ErrInvalidProjectID = errors.New("id de proyecto inválido")
)

func getWorkspaceIDFromRequest(r *http.Request) (uint64, error) {
	id, err := strconv.ParseUint(
		middleware.GetContextParams(r).Chi.URLParam("workspace-id"),
		10,
		64)
	if err != nil {
		return 0, errors.New("id de workspace inválido")
	}
	return id, nil
}

func getProjectIDFromRequest(r *http.Request) (uint64, error) {
	id, err := strconv.ParseUint(
		middleware.GetContextParams(r).Chi.URLParam("project-id"),
		10,
		64)
	if err != nil {
		return 0, errors.New("id de projecto inválido")
	}
	return id, nil
}

func getAccessPointIDFromRequest(r *http.Request) (uint64, error) {
	id, err := strconv.ParseUint(
		middleware.GetContextParams(r).Chi.URLParam("access-point"),
		10,
		64)
	if err != nil {
		return 0, errors.New("id de access point inválido")
	}
	return id, nil
}
