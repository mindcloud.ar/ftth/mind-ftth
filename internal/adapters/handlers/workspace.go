package handlers

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"

	"gitlab.com/mindcloud.ar/dev-tools/go-tools/middleware"
	"gitlab.com/mindcloud.ar/dev-tools/go-tools/render"

	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/domain"
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/ports"
)

// NewGroupHandler retorna un handler para ABM de Grupos
func NewGroupHandler(srv ports.WorkspaceServicer) chi.Router {

	return WorkspaceHandler{
		service: srv,
	}.routes()
}

// WorkspaceHandler Estructura con lo métodos para la recepción de los requests al recurso
type WorkspaceHandler struct {
	service ports.WorkspaceServicer
}

func (h WorkspaceHandler) routes() chi.Router {
	r := chi.NewRouter()

	r.Get("/", h.GetAll)
	r.Post("/", h.Create)

	r.Route("/{group-id}", func(r chi.Router) {
		r.Get("/", h.GetOne)
		r.Patch("/", h.Update)
		r.Put("/", h.Replace)
		r.Delete("/", h.Delete)

	})

	return r
}

func (h WorkspaceHandler) getGroupIDFromRequest(r *http.Request) (uint64, error) {
	id, err := strconv.ParseUint(
		middleware.GetContextParams(r).Chi.URLParam("group-id"),
		10,
		64)
	if err != nil {
		return 0, errors.New("id de grupo inválido")
	}
	return id, nil
}

func (h WorkspaceHandler) GetAll(w http.ResponseWriter, r *http.Request) {
	groups, err := h.service.GetAll()
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(groups))
}

func (h WorkspaceHandler) GetOne(w http.ResponseWriter, r *http.Request) {
	id, err := h.getGroupIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	group, err := h.service.GetById(id)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(group))
}

func (h WorkspaceHandler) Update(w http.ResponseWriter, r *http.Request) {
	rp := middleware.GetContextParams(r)
	id, err := h.getGroupIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	var payload map[string]any
	if err := json.Unmarshal(rp.RequestBody, &payload); err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	prj, err := h.service.Update(id, payload)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(prj))

}

func (h WorkspaceHandler) Replace(w http.ResponseWriter, r *http.Request) {
	rp := middleware.GetContextParams(r)
	id, err := h.getGroupIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	payload := domain.Workspace{}
	if err := json.Unmarshal(rp.RequestBody, &payload); err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}
	payload.ID = &id

	prj, err := h.service.Replace(payload)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(prj))

}

func (h WorkspaceHandler) Create(w http.ResponseWriter, r *http.Request) {
	rp := middleware.GetContextParams(r)
	workspaces := []domain.Workspace{}
	if err := json.Unmarshal(rp.RequestBody, &workspaces); err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	result, err := h.service.Create(workspaces)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		log.Error().Err(err).Send()
		return
	}

	render.Render(w, r, render.NewSuccessResponse(result))
}

func (h WorkspaceHandler) Delete(w http.ResponseWriter, r *http.Request) {
	id, err := h.getGroupIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	err = h.service.Delete(id)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}
	render.Render(w, r, render.NewSuccessResponse("grupo eliminado"))

}
