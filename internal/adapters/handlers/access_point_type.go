package handlers

import (
	"net/http"

	"github.com/go-chi/chi"

	"gitlab.com/mindcloud.ar/dev-tools/go-tools/render"

	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/ports"
)

// NewAccessPointTypeHandler retorna un handler para ABM de tipos de puntos de acceso
func NewAccessPointTypeHandler(srv ports.AccessPointTypeServicer) chi.Router {

	return AccessPointTypeHandler{
		service: srv,
	}.Routes()
}

// ProjectHandler Estructura con lo métodos para la recepción de los requests al recurso
type AccessPointTypeHandler struct {
	service ports.AccessPointTypeRepositorier
}

func (h AccessPointTypeHandler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Get("/", h.GetAll)

	return r
}

func (h AccessPointTypeHandler) GetAll(w http.ResponseWriter, r *http.Request) {
	projects, err := h.service.GetAll()

	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}
	render.Render(w, r, render.NewSuccessResponse(projects))
}
