package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi"
	_log "github.com/rs/zerolog/log"

	"gitlab.com/mindcloud.ar/dev-tools/go-tools/middleware"
	"gitlab.com/mindcloud.ar/dev-tools/go-tools/render"

	// "gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/domain"
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/domain"
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/ports"
)

var log = _log.With().Str("pkg", "handlers").Logger()

// NewProjectHandler retorna un handler para ABM de Projectos
func NewProjectHandler(srv ports.ProjectServicer) chi.Router {

	return ProjectHandler{
		service: srv,
	}.Routes()
}

// ProjectHandler Estructura con lo métodos para la recepción de los requests al recurso
type ProjectHandler struct {
	service ports.ProjectServicer
}

func (h ProjectHandler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Get("/", h.GetAll)
	r.Post("/", h.Create)

	r.Route("/{project-id}", func(r chi.Router) {
		r.Get("/", h.Get)
		r.Patch("/", h.Update)
		r.Put("/", h.Replace)
		r.Delete("/", h.Delete)

	})

	return r
}

func (h ProjectHandler) GetAll(w http.ResponseWriter, r *http.Request) {

	var projects []domain.Project
	var err error

	grp_id, err := getWorkspaceIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	projects, err = h.service.GetAll(grp_id)

	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}
	render.Render(w, r, render.NewSuccessResponse(projects))
}

func (h ProjectHandler) Get(w http.ResponseWriter, r *http.Request) {
	grp_id, err := getWorkspaceIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	prj_id, err := getProjectIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	prj, err := h.service.Get(grp_id, prj_id)
	if err != nil {
		log.Error().Err(err).Send()
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(prj))
}

func (h ProjectHandler) Update(w http.ResponseWriter, r *http.Request) {
	rp := middleware.GetContextParams(r)
	grp_id, err := getWorkspaceIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	prj_id, err := getProjectIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	var payload map[string]any
	if err := json.Unmarshal(rp.RequestBody, &payload); err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	prj, err := h.service.Update(grp_id, prj_id, payload)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(prj))

}

func (h ProjectHandler) Replace(w http.ResponseWriter, r *http.Request) {
	rp := middleware.GetContextParams(r)
	grp_id, err := getWorkspaceIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	prj_id, err := getProjectIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	payload := domain.Project{}
	if err := json.Unmarshal(rp.RequestBody, &payload); err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	prj, err := h.service.Replace(grp_id, prj_id, payload)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(prj))

}

func (h ProjectHandler) Create(w http.ResponseWriter, r *http.Request) {
	rp := middleware.GetContextParams(r)

	workspace, err := getWorkspaceIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	projects := []domain.Project{}
	if err := json.Unmarshal(rp.RequestBody, &projects); err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
	}

	// projects.WorkspaceID = grp_id

	result, err := h.service.Create(workspace, projects)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		log.Error().Err(err).Send()
		return
	}

	render.Render(w, r, render.NewSuccessResponse(result))
}

func (h ProjectHandler) Delete(w http.ResponseWriter, r *http.Request) {
	grp_id, err := getWorkspaceIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	prj_id, err := getProjectIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	err = h.service.Delete(grp_id, prj_id)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}
	render.Render(w, r, render.NewSuccessResponse("proyecto eliminado"))

}
