package handlers

import (
	// "errors"
	"encoding/json"
	"net/http"
	// "strconv"

	"github.com/go-chi/chi"

	"gitlab.com/mindcloud.ar/dev-tools/go-tools/middleware"
	"gitlab.com/mindcloud.ar/dev-tools/go-tools/render"
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/domain"

	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/ports"
)

// NewAccessPointHandler retorna un handler para ABM de tipos de puntos de acceso
func NewAccessPointHandler(srv ports.AccessPointServicer) chi.Router {

	return AccessPointHandler{
		service: srv,
	}.Routes()
}

// ProjectHandler Estructura con lo métodos para la recepción de los requests al recurso
type AccessPointHandler struct {
	service ports.AccessPointServicer
}

func (h AccessPointHandler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Get("/", h.GetAll)
	r.Post("/", h.Create)

	r.Route("/{access-point}", func(r chi.Router) {
		r.Get("/", h.Get)
		r.Patch("/", h.Update)
		// r.Put("/", h.Replace)
		// r.Delete("/", h.Delete)

	})

	return r
}

func (h AccessPointHandler) GetAll(w http.ResponseWriter, r *http.Request) {
	grp_id, err := getWorkspaceIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	prj_id, err := getProjectIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	ap, err := h.service.GetAll(grp_id, prj_id)

	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}
	render.Render(w, r, render.NewSuccessResponse(ap))
}

func (h AccessPointHandler) Get(w http.ResponseWriter, r *http.Request) {
	grp_id, err := getWorkspaceIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	prj_id, err := getProjectIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	apID, err := getAccessPointIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	prj, err := h.service.Get(grp_id, prj_id, apID)
	if err != nil {
		log.Error().Err(err).Send()
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(prj))
}

func (h AccessPointHandler) Create(w http.ResponseWriter, r *http.Request) {
	rp := middleware.GetContextParams(r)

	workspace, err := getWorkspaceIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	project, err := getProjectIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	ap := []domain.AccessPoint{}
	if err := json.Unmarshal(rp.RequestBody, &ap); err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
	}

	result, err := h.service.Create(workspace, project, ap)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		log.Error().Err(err).Send()
		return
	}

	render.Render(w, r, render.NewSuccessResponse(result))
}

func (h AccessPointHandler) Update(w http.ResponseWriter, r *http.Request) {
	rp := middleware.GetContextParams(r)

	workspace, err := getWorkspaceIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	project, err := getProjectIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	apID, err := getAccessPointIDFromRequest(r)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		return
	}

	var data map[string]any
	if err := json.Unmarshal(rp.RequestBody, &data); err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
	}

	result, err := h.service.Update(workspace, project, apID, data)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err))
		log.Error().Err(err).Send()
		return
	}

	render.Render(w, r, render.NewSuccessResponse(result))
}
