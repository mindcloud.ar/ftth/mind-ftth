// package project es el repositorio(store) para projectos
package project

import (
	"errors"
	"fmt"
	"strings"

	"github.com/go-sqlx/sqlx"
	_log "github.com/rs/zerolog/log"

	"gitlab.com/mindcloud.ar/dev-tools/go-tools/dbhelper"
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/domain"
)

var log = _log.With().Str("pkg", "repository").Logger()

var (
	ErrInvalidProjectID = errors.New("el id de proyecto especificaco no es válido")
	ErrNoAffectedRows   = errors.New("no se afectaron datos en la BD")
)

var (
	sqlSelectProject = fmt.Sprintf("SELECT %s FROM `%s`", strings.Join(projectFields, ", "), tableName)
)

// ProjectMySQLRepository define un servicio de projecto
type ProjectMySQLRepository struct {
	db *sqlx.DB
}

// NewProjectMysqlRepository devuelve un nuevo repositorio de projecto para MySQL
func NewProjectMysqlRepository(dbInstance *sqlx.DB) *ProjectMySQLRepository {
	return &ProjectMySQLRepository{
		db: dbInstance,
	}
}

// GetProjects devuelve la lista de projectos
func (r *ProjectMySQLRepository) GetProjects(contains string, parentId *uint64, limit, offset uint64) ([]domain.Project, error) {
	projects := []domain.Project{}

	var sql = sqlSelectProject

	var conds []string
	var args []any

	if contains != "" {
		conds = append(conds, "(name like ? or description like ?)")
		args = append(args, "%"+contains+"%")
		args = append(args, "%"+contains+"%")
	}

	if parentId == nil {
		conds = append(conds, "parent_id IS NULL")
	} else {
		conds = append(conds, "parent_id = ?")
		args = append(args, parentId)
	}

	if len(conds) > 0 {
		sql += " WHERE " + strings.Join(conds, " AND ")
	}

	sql += fmt.Sprintf(" ORDER BY `%s` ", identifier)

	if limit > 0 {
		sql = fmt.Sprintf("%s LIMIT %d", sql, limit)
		if offset > 0 {
			sql = fmt.Sprintf("%s OFFSET %d", sql, limit)
		}
	}

	log.Trace().Str("sql", sql).Interface("args", args).Send()

	if err := r.db.Select(&projects, sql, args...); err != nil {
		return nil, err
	}

	return projects, nil
}

func (r *ProjectMySQLRepository) GetProject(id uint64) (*domain.Project, error) {

	model := &domain.Project{}

	if id == 0 {
		return nil, ErrInvalidProjectID
	}

	sql := fmt.Sprintf("%s WHERE `%s` = ?", sqlSelectProject, identifier)

	log.Trace().Str("sql", sql).Interface("id", id).Send()

	if err := r.db.Get(model, sql, id); err != nil {
		return nil, err
	}

	return model, nil
}

func (r *ProjectMySQLRepository) CreateProject(project domain.Project) (*domain.Project, error) {

	id, err := dbhelper.NewQuery(r.db).
		Insert(project).
		Columns(`name`, `parent_id`, `description`, `lat`, `long`).
		Into(tableName).
		Exec()

	if err != nil {
		return nil, err
	}

	prj, err := r.GetProject(uint64(id))
	if err != nil {
		return nil, err
	}

	return prj, nil
}

// SaveProject Reemplaza el contenido completo de un proyecto con los datos especificados
func (r *ProjectMySQLRepository) SaveProject(data domain.Project) (*domain.Project, error) {
	res, err := r.db.NamedExec(
		fmt.Sprintf("UPDATE `%s` set `name` = :name, `parent_id` = :parent_id, `description` = :description, `lat` = :lat, `long` = :long where `id` = :id",
			tableName),
		data)

	if err != nil {
		return nil, err
	}
	a, err := res.RowsAffected()

	if a == 0 {
		return nil, ErrNoAffectedRows
	}

	if err != nil {
		return nil, err
	}

	prj, err := r.GetProject(*data.ID)
	if err != nil {
		return nil, err
	}

	return prj, nil
}

func (r *ProjectMySQLRepository) DeleteProject(id uint64) error {
	res, err := r.db.Exec(fmt.Sprintf("DELETE FROM `%s` where id = ?", tableName), id)
	if err != nil {
		return err
	}

	if a, err := res.RowsAffected(); a == 0 || err != nil {
		return ErrNoAffectedRows
	}

	return nil
}
