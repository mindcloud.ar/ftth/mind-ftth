package project

import (
	"errors"

	"gitlab.com/mindcloud.ar/dev-tools/go-tools/dbhelper"
)

var (
	tableName     = "project"
	identifier    = "id"
	projectFields = dbhelper.SanitizeFields("mysql", []string{
		"id",
		"name",
		"description",
		"lat",
		"long",
		"parent_id",
	})
)

var (
	ErrProjectNotFound = errors.New("el proyecto especificado no existe")
)
