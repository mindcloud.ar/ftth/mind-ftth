package project

import (
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/domain"
)

// NewProjectMysqlRepository devuelve un nuevo repositorio de projecto para MySQL
func NewProjectMockRepository() *ProjectMockRepository {
	repo := &ProjectMockRepository{
		db: []domain.Project{},
	}

	repo.addMockedProject("Projecto 1", "Descripcion 1", 1, -34.61855253109212, -58.652100563049316, 16)

	return repo
}

type ProjectMockRepository struct {
	db []domain.Project
}

func (r *ProjectMockRepository) addMockedProject(name, description string, workspaceID uint64, lat, long float64, zoom int) domain.Project {
	p := domain.Project{
		ID:          r.nextID(),
		Name:        name,
		Description: description,
		WorkspaceID: workspaceID,
		MapPoint: domain.MapPoint{
			Lat: &lat,
			Lng: &long,
		},
		Zoom: zoom,
	}

	r.db = append(r.db, p)

	return p
}

func (r *ProjectMockRepository) nextID() *uint64 {
	var max uint64 = 0
	for _, p := range r.db {
		if p.ID != nil && *p.ID > max {
			max = *p.ID
		}
	}

	max++

	return &max
}

// GetAll Devuelve la lista de proyectos.
func (r *ProjectMockRepository) GetAll(groupID uint64) ([]domain.Project, error) {
	result := []domain.Project{}

	for _, p := range r.db {

		if p.WorkspaceID == groupID {
			result = append(result, p)
		}
	}

	return result, nil
}

func (r *ProjectMockRepository) Get(id uint64) (*domain.Project, error) {
	for _, p := range r.db {
		if *p.ID == id {
			return &p, nil
		}
	}

	return nil, ErrProjectNotFound
}

func (r *ProjectMockRepository) Create(data domain.Project) (*domain.Project, error) {
	log.Debug().Interface("data", data).Msg("creando proyecto")

	var lng, lat float64
	if data.Lat != nil {
		lat = *data.Lat
	}
	if data.Lng != nil {
		lng = *data.Lng
	}

	p := r.addMockedProject(data.Name, data.Description, data.WorkspaceID, lat, lng, data.Zoom)

	return &p, nil
}
func (r *ProjectMockRepository) Save(data domain.Project) (*domain.Project, error) {

	for i, p := range r.db {
		if *p.ID == *data.ID {
			r.db[i] = data
			return &data, nil
		}
	}

	return nil, ErrProjectNotFound
}
func (r *ProjectMockRepository) Delete(id uint64) error {
	prj, err := r.Get(id)
	if err != nil {
		return err
	}

	for i, p := range r.db {
		if *p.ID == *prj.ID {
			r.db = append(r.db[0:i], r.db[i+1:]...)
			return nil
		}
	}

	return ErrProjectNotFound
}
