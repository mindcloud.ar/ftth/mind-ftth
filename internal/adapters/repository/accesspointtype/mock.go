package accesspointtype

import (
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/domain"
)

func NewAccessPointTypeMockRepository() *AccessPointTypeMockRepository {
	repo := &AccessPointTypeMockRepository{
		db: []domain.AccessPointType{},
	}

	t1 := repo.newMockAPT("nodo", "Nodo", "Nodo Principal del proyecto")
	t2 := repo.newMockAPT("ce", "Caja de Empalme", "Caja de Empalme")
	t3 := repo.newMockAPT("cto", "CTO", "Caja de Distribución")
	t4 := repo.newMockAPT("customer", "Abonado", "Abonado")

	repo.db = append(repo.db, t1)
	repo.db = append(repo.db, t2)
	repo.db = append(repo.db, t3)
	repo.db = append(repo.db, t4)

	return repo
}

type AccessPointTypeMockRepository struct {
	db []domain.AccessPointType
}

func (r *AccessPointTypeMockRepository) newMockAPT(id, name, description string) domain.AccessPointType {

	return domain.AccessPointType{
		ID:          &id,
		Name:        name,
		Description: description,
	}
}

// GetAll Devuelve la lista de tipos de puntos de acceso.
func (r *AccessPointTypeMockRepository) GetAll() ([]domain.AccessPointType, error) {

	return r.db, nil
}
