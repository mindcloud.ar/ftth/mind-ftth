package group

import "errors"

var (
	ErrGroupNotFound = errors.New("el grupo especificado no existe")
)
