package group

import (
	// "github.com/rs/zerolog/log"
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/domain"
)

func NewWorkspaceMockRepository() *GroupMockRepository {
	repo := &GroupMockRepository{
		db: []domain.Workspace{},
	}

	repo.addMockedGroup("Un workspace", "Un worskspace de prueba", 1, -34.60947549180878, -58.62201690673829, 12, false)

	return repo
}

type GroupMockRepository struct {
	db []domain.Workspace
}

func (r *GroupMockRepository) addMockedGroup(name string, description string, owner uint64, lat, lng float64, zoom int, archived bool) domain.Workspace {
	ap := domain.Workspace{
		ID:          r.nextID(),
		Name:        name,
		Description: description,
		Owner:       owner,
		MapPoint: domain.MapPoint{
			Lat: &lat,
			Lng: &lng,
		},
		Zoom:     zoom,
		Archived: archived,
	}
	r.db = append(r.db, ap)
	return ap
}

func (r *GroupMockRepository) nextID() *uint64 {
	var max uint64 = 0
	for _, p := range r.db {
		if p.ID != nil && *p.ID > max {
			max = *p.ID
		}
	}
	max++

	return &max
}

// GetAll Devuelve la lista de tipos de puntos de acceso.
func (r *GroupMockRepository) Select() ([]domain.Workspace, error) {

	return r.db, nil
}

func (r *GroupMockRepository) Get(id uint64) (*domain.Workspace, error) {
	for _, ap := range r.db {
		if *ap.ID == id {
			return &ap, nil
		}
	}
	return nil, ErrGroupNotFound
}

func (r *GroupMockRepository) Create(data domain.Workspace) (*domain.Workspace, error) {
	data.ID = r.nextID()
	r.db = append(r.db, data)
	return &data, nil
}

func (r *GroupMockRepository) Delete(id uint64) error {
	prj, err := r.Get(id)
	if err != nil {
		return err
	}

	for i, p := range r.db {
		if *p.ID == *prj.ID {
			r.db = append(r.db[0:i], r.db[i+1:]...)
			return nil
		}
	}

	return ErrGroupNotFound

}

func (r *GroupMockRepository) Save(data domain.Workspace) (*domain.Workspace, error) {

	for i, p := range r.db {
		if *p.ID == *data.ID {
			r.db[i] = data
			return &data, nil
		}
	}

	return nil, ErrGroupNotFound

}
