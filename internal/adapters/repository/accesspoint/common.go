package accesspoint

import (
	"errors"
)

var (
	ErrAccessPointNotFound = errors.New("el punto de acceso especificado no existe o pertenece a otro proyecto")
)
