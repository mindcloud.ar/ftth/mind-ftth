package accesspoint

import (
	// "github.com/rs/zerolog/log"
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/domain"
)

func NewAccessPointMockRepository() *AccessPointMockRepository {
	repo := &AccessPointMockRepository{
		db: []domain.AccessPoint{},
	}

	repo.addMockAccessPoint("Nodo", 1, "node", -34.61743119962527, -58.65175724029542)
	repo.addMockAccessPoint("Caja 1", 1, "ce", -34.616813136435084, -58.651092052459724)
	repo.addMockAccessPoint("Caja 2", 1, "cto", -34.61623038692614, -58.65038394927979)
	repo.addMockAccessPoint("Caja 3", 1, "cto", -34.615656462956395, -58.652583360672004)
	repo.addMockAccessPoint("Abonado", 1, "customer", -34.61753273813778, -58.65017473697662)

	return repo
}

type AccessPointMockRepository struct {
	db []domain.AccessPoint
}

func (r *AccessPointMockRepository) addMockAccessPoint(name string, projectID uint64, apType string, lat, long float64) domain.AccessPoint {
	ap := domain.AccessPoint{
		ID:        r.nextID(),
		ProjectID: projectID,
		Name:      name,
		Type:      apType,
		MapPoint: domain.MapPoint{
			Lat: &lat,
			Lng: &long,
		},
	}
	r.db = append(r.db, ap)
	return ap
}

func (r *AccessPointMockRepository) nextID() *uint64 {
	var max uint64 = 0
	for _, p := range r.db {
		if p.ID != nil && *p.ID > max {
			max = *p.ID
		}
	}
	max++

	return &max
}

// GetAll Obtiene la lista de puntos de acceso de un proyecto
func (r *AccessPointMockRepository) GetAll(projectID uint64) ([]domain.AccessPoint, error) {
	var result []domain.AccessPoint

	for _, ap := range r.db {
		if ap.ProjectID == projectID {
			result = append(result, ap)
		}
	}

	return result, nil
}

func (r *AccessPointMockRepository) Get(id uint64) (*domain.AccessPoint, error) {
	for _, ap := range r.db {
		if *ap.ID == id {
			return &ap, nil
		}
	}
	return nil, ErrAccessPointNotFound
}

func (r *AccessPointMockRepository) Create(data domain.AccessPoint) (*domain.AccessPoint, error) {
	p := r.addMockAccessPoint(data.Name, data.ProjectID, data.Type, *data.Lat, *data.Lng)

	return &p, nil
}
func (r *AccessPointMockRepository) Save(data domain.AccessPoint) (*domain.AccessPoint, error) {

	for i, p := range r.db {
		if *p.ID == *data.ID {
			r.db[i] = data
			return &data, nil
		}
	}

	return nil, ErrAccessPointNotFound
}

func (r *AccessPointMockRepository) Delete(id uint64) error {
	prj, err := r.Get(id)
	if err != nil {
		return err
	}

	for i, p := range r.db {
		if *p.ID == *prj.ID {
			r.db = append(r.db[0:i], r.db[i+1:]...)
			return nil
		}
	}

	return ErrAccessPointNotFound
}
