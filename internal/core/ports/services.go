package ports

import (
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/domain"
)

// WorkspaceServicer representa un servicio para administrar grupos de proyectos
type WorkspaceServicer interface {
	GetAll() ([]domain.Workspace, error)
	GetById(id uint64) (*domain.Workspace, error)
	Create(data []domain.Workspace) ([]domain.Workspace, error)
	Delete(id uint64) error
	Replace(data domain.Workspace) (*domain.Workspace, error)
	Update(id uint64, data map[string]any) (*domain.Workspace, error)
	// TODO: implementar los siguientes métodos
	// GetByName(name string) (*domain.Group, error)
	// Archive(id uint64) error
	// GetOwner(groupID uint64) (domain.User, error)
	// TransferOwner(groupID uint64, newOwner uint64) error
}

type ProjectServicer interface {
	GetAll(groupID uint64) ([]domain.Project, error)
	Get(groupID uint64, id uint64) (*domain.Project, error)
	Create(workspaceID uint64, data []domain.Project) ([]domain.Project, error)
	Replace(groupID uint64, id uint64, data domain.Project) (*domain.Project, error)
	Update(groupID uint64, id uint64, data map[string]any) (*domain.Project, error)
	Delete(groupID uint64, id uint64) error
}

type AccessPointTypeServicer interface {
	GetAll() ([]domain.AccessPointType, error)
}

type AccessPointServicer interface {
	GetAll(groupID uint64, projectID uint64) ([]domain.AccessPoint, error)
	Get(groupID uint64, projectID uint64, id uint64) (*domain.AccessPoint, error)
	Create(groupID uint64, projectID uint64, data []domain.AccessPoint) ([]domain.AccessPoint, error)
	Delete(groupID uint64, projectID uint64, id uint64) error
	Replace(groupID uint64, projectID uint64, id uint64, data domain.AccessPoint) (*domain.AccessPoint, error)
	Update(groupID uint64, projectID uint64, id uint64, data map[string]any) (*domain.AccessPoint, error)
}
