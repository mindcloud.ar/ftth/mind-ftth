package ports

import (
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/domain"
)

type WorkspaceRepositorier interface {
	// Select obtiene la lista de grupos de proyectos
	Select() ([]domain.Workspace, error)

	// Get obtiene un grupo de proyecto
	Get(id uint64) (*domain.Workspace, error)

	// Create crea un grupo de proyectos
	Create(data domain.Workspace) (*domain.Workspace, error)

	// Save guarda un grupo de proyectos existente
	Save(data domain.Workspace) (*domain.Workspace, error)

	// Delete elimina un grupo de proyectos
	Delete(id uint64) error
}

type ProjectRepositorier interface {
	GetAll(groupID uint64) ([]domain.Project, error)
	Get(id uint64) (*domain.Project, error)
	Create(data domain.Project) (*domain.Project, error)
	Save(data domain.Project) (*domain.Project, error)
	Delete(id uint64) error
}

type AccessPointTypeRepositorier interface {
	GetAll() ([]domain.AccessPointType, error)
}

type AccessPointRepositorier interface {
	GetAll(projectID uint64) ([]domain.AccessPoint, error)
	Get(id uint64) (*domain.AccessPoint, error)
	Create(data domain.AccessPoint) (*domain.AccessPoint, error)
	Save(data domain.AccessPoint) (*domain.AccessPoint, error)
	Delete(id uint64) error
}
