package services

import (
	"encoding/json"

	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/domain"
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/ports"
)

// GroupService define un servicio de grupo de proyectos
type GroupService struct {
	repo ports.WorkspaceRepositorier
}

// NewWorkspaceService devuelve un nuevo servicio de grupo de proyectos
func NewWorkspaceService(repo ports.WorkspaceRepositorier) *GroupService {
	return &GroupService{
		repo: repo,
	}
}

func (s *GroupService) GetAll() ([]domain.Workspace, error) {
	return s.repo.Select()

}
func (s *GroupService) GetById(id uint64) (*domain.Workspace, error) {
	return s.repo.Get(id)
}
func (s *GroupService) Create(workspaces []domain.Workspace) ([]domain.Workspace, error) {
	var result []domain.Workspace
	for _, ws := range workspaces {
		r, err := s.repo.Create(ws)
		if err != nil {
			return nil, err
		}

		result = append(result, *r)
	}
	return result, nil
}
func (s *GroupService) Delete(id uint64) error {
	return s.repo.Delete(id)
}
func (s *GroupService) Replace(data domain.Workspace) (*domain.Workspace, error) {
	return s.repo.Save(data)
}
func (s *GroupService) Update(id uint64, data map[string]any) (*domain.Workspace, error) {
	g, err := s.repo.Get(id)
	if err != nil {
		return nil, err
	}

	bytes, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(bytes, g); err != nil {
		return nil, err
	}

	return s.repo.Save(*g)
}
