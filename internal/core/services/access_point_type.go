package services

import (
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/domain"
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/ports"
)

// NewAccessPointTypeService devuelve un nuevo servicio
func NewAccessPointTypeService(repo ports.AccessPointTypeRepositorier) *AccessPointTypeService {
	return &AccessPointTypeService{
		repo: repo,
	}
}

type AccessPointTypeService struct {
	repo ports.AccessPointTypeRepositorier
}

func (s *AccessPointTypeService) GetAll() ([]domain.AccessPointType, error) {
	return s.repo.GetAll()
}
