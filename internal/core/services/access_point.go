package services

import (
	"encoding/json"
	"errors"

	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/domain"
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/ports"
)

var (
	ErrAccessPointNotFound = errors.New("el access point no existe o pertenece a otro proyecto")
)

// NewAccessPointService devuelve un nuevo servicio de projecto
func NewAccessPointService(accessPointRepo ports.AccessPointRepositorier, projectRepo ports.ProjectRepositorier) *AccessPointService {
	return &AccessPointService{
		accessPointRepo: accessPointRepo,
		projectRepo:     projectRepo,
	}
}

type AccessPointService struct {
	accessPointRepo ports.AccessPointRepositorier
	projectRepo     ports.ProjectRepositorier
}

func (s *AccessPointService) getProject(workspaceID uint64, projectID uint64) (*domain.Project, error) {
	prj, err := s.projectRepo.Get(projectID)
	if err != nil {
		return nil, err
	}

	if prj.WorkspaceID != workspaceID {
		return nil, ErrProjectNotFound
	}
	return prj, nil
}

func (s *AccessPointService) GetAll(workspaceID uint64, projectID uint64) ([]domain.AccessPoint, error) {
	_, err := s.getProject(workspaceID, projectID)
	if err != nil {
		return nil, err
	}

	return s.accessPointRepo.GetAll(projectID)
}

func (s *AccessPointService) Get(workspaceID uint64, projectID uint64, id uint64) (*domain.AccessPoint, error) {
	_, err := s.getProject(workspaceID, projectID)
	if err != nil {
		return nil, err
	}

	ap, err := s.accessPointRepo.Get(id)
	if err != nil {
		return nil, err
	}

	if ap.ProjectID != projectID {
		return nil, ErrAccessPointNotFound
	}

	return ap, nil
}

func (s *AccessPointService) Create(workspaceID uint64, projectID uint64, data []domain.AccessPoint) ([]domain.AccessPoint, error) {
	prj, err := s.getProject(workspaceID, projectID)
	if err != nil {
		return nil, err
	}
	if prj.WorkspaceID != workspaceID {
		return nil, errors.New("workspace inválido o el proyecto no pertenece al workspace")
	}

	var result []domain.AccessPoint
	for _, ap := range data {
		ap.ProjectID = projectID

		r, err := s.accessPointRepo.Create(ap)
		if err != nil {
			return nil, err
		}

		result = append(result, *r)
	}
	return result, nil

}

func (s *AccessPointService) Delete(workspaceID uint64, projectID uint64, id uint64) error {
	_, err := s.Get(workspaceID, projectID, id)
	if err != nil {
		return err
	}

	return s.accessPointRepo.Delete(id)
}

func (s *AccessPointService) Replace(workspaceID uint64, projectID uint64, id uint64, data domain.AccessPoint) (*domain.AccessPoint, error) {
	_, err := s.Get(workspaceID, projectID, id)
	if err != nil {
		return nil, err
	}

	data.ID = &id
	data.ProjectID = projectID

	return s.accessPointRepo.Save(data)
}

func (s *AccessPointService) Update(workspaceID uint64, projectID uint64, id uint64, data map[string]any) (*domain.AccessPoint, error) {
	prj, err := s.Get(workspaceID, projectID, id)
	if err != nil {
		return nil, err
	}

	// no permito mofificar ni el id ni el projecto
	delete(data, "id")
	delete(data, "projectID")

	bytes, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(bytes, prj); err != nil {
		return nil, err
	}

	return s.accessPointRepo.Save(*prj)
}
