package services

import (
	// "encoding/json"

	"encoding/json"
	"errors"

	// "github.com/rs/zerolog/log"
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/domain"
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/ports"
)

var (
	ErrProjectNotFound = errors.New("el proyecto especificado no existe o pertenece a otro grupo")
)

// ProjectService define un servicio de projecto
type ProjectService struct {
	repo ports.ProjectRepositorier
}

// NewProjectService devuelve un nuevo servicio de projecto
func NewProjectService(repo ports.ProjectRepositorier) *ProjectService {
	return &ProjectService{
		repo: repo,
	}
}

// GetAll devuelve la lista de projectos
func (p *ProjectService) GetAll(groupID uint64) ([]domain.Project, error) {
	return p.repo.GetAll(groupID)
}

func (p *ProjectService) Get(groupID uint64, id uint64) (*domain.Project, error) {
	prj, err := p.repo.Get(id)
	if err != nil {
		return nil, ErrProjectNotFound
	}
	if prj.WorkspaceID != groupID {
		return nil, ErrProjectNotFound
	}

	return prj, nil
}

func (p *ProjectService) Create(workspaceID uint64, projects []domain.Project) ([]domain.Project, error) {

	var result []domain.Project
	for _, prj := range projects {
		prj.WorkspaceID = workspaceID
		r, err := p.repo.Create(prj)
		if err != nil {
			return nil, err
		}

		result = append(result, *r)
	}
	return result, nil
}

func (p *ProjectService) Replace(groupID uint64, id uint64, data domain.Project) (*domain.Project, error) {
	prj, err := p.Get(data.WorkspaceID, *data.ID)
	if err != nil {
		return nil, err
	}

	data.ID = prj.ID
	data.WorkspaceID = prj.WorkspaceID

	return p.repo.Save(data)

}

func (p *ProjectService) Update(groupID uint64, id uint64, data map[string]any) (*domain.Project, error) {
	prj, err := p.Get(groupID, id)

	if err != nil {
		return nil, err
	}

	// no permito mofificar ni el id ni el grupo
	delete(data, "id")
	delete(data, "workspaceID")

	bytes, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(bytes, prj); err != nil {
		return nil, err
	}

	return p.repo.Save(*prj)
}

func (p *ProjectService) Delete(groupID uint64, id uint64) error {
	_, err := p.Get(groupID, id)
	if err != nil {
		return err
	}

	return p.repo.Delete(id)
}
