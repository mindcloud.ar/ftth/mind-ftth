package domain

// Workspace representa a un grupo de proyectos
type Workspace struct {
	ID          *uint64 `json:"id" db:"id"`
	Name        string  `json:"name" db:"name"`
	Description string  `json:"description" db:"description"`
	Owner       uint64  `json:"-" db:"owner"`
	Archived    bool    `json:"archived" db:"archived"`
	Zoom        int     `json:"zoom" db:"zoom"`
	MapPoint
}

// Project representa a un proyecto
type Project struct {
	ID          *uint64 `json:"id" db:"id"`
	Name        string  `json:"name" db:"name"`
	Description string  `json:"description" db:"description"`
	WorkspaceID uint64  `json:"workspaceID" db:"workspace_id"`
	Zoom        int     `json:"zoom" db:"zoom"`
	MapPoint
}

// MapPoint representa un punto en el mapa
type MapPoint struct {
	Lat *float64 `json:"lat" db:"lat"`
	Lng *float64 `json:"lng" db:"lng"`
}

// AccessPointType represente los tipos de puntos de accesos válidos
type AccessPointType struct {
	ID          *string `json:"id" db:"id"`
	Name        string  `json:"name" db:"name"`
	Description string  `json:"description" db:"description"`
}

// AccessPoint representa un punto de acceso en el mapa
type AccessPoint struct {
	ID        *uint64 `json:"id" db:"id"`
	Name      string  `json:"name" db:"name"`
	ProjectID uint64  `json:"projectID" db:"project_id"`
	Type      string  `json:"type" db:"type"`
	MapPoint
}
