import * as path from 'path';

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  server: {
    port: 3001,
    host: "0.0.0.0",
    proxy: {
      '/ping': {
        target: 'http://localhost:3002',
        changeOrigin: true,
        secure: false,
       },
      '/api': {
        target: 'http://localhost:3002',
        changeOrigin: true,
        secure: false,
        // rewrite: (path) => path.replace(/^\/api/, ''),
        ws: true,
       }
    }
  },
  resolve: {
    alias: {
      '@mind-vue3-tools': path.resolve(__dirname, 'src/../modules/mind-vue3-tools/src'),
      '@': path.resolve(__dirname, 'src'),
      '@services': path.resolve(__dirname, 'src/services'),
      '@repositories': path.resolve(__dirname, 'src/repositories'),
      '@components': path.resolve(__dirname, 'src/components'),
      '@composables': path.resolve(__dirname, 'src/composables'),
      '@assets': path.resolve(__dirname, 'src/assets'),
    }
  },
  build: {
    commonjsOptions: {
      transformMixedEsModules: true,
    }
  }

})
