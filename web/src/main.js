import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import mind from '@mind-vue3-tools'
import router from './router'

const app = createApp(App)
app.use(mind)
app.use(router)
app.mount('#app')

