import { ref, computed } from 'vue'
import {NewProjectRestRepository} from '@/repositories/project/rest.js'
import {NewProjectService} from '@/services/project.js'


/**
 * composable para workspaces
 * @returns Object
 */
export function useProject() {

  const state = ref([])
  const repo = NewProjectRestRepository({uri: '/api/workspaces'})
  const service = NewProjectService(repo)

  async function fetchProjects(workspaceID) {
    try {
      const result = await service.getAll(workspaceID)
      state.value = result
    } catch(err) {
      console.log("composable", err)
      throw(err)
    }
  }

  async function deleteProject(id) {
    const response = await service.delete(id)

    // eliminar el elemento de la lista
    const idx = state.value.findIndex(el => el.id == id)
    if (idx >= 0) {
      state.value.splice(idx, 1)
    }

    return response
  }


  const updateProject = async (data) => {
    try {
      const response = await service.update(data)
      return response
    } catch(e) {
      console.error("error al actualizar proyecto", e)
      throw(e)
    }
  }

  const createProject = async (data) => {
    console.log(data)
    const result = await service.create(data)
    state.value.push(...result)
    return result
  }


  const newEmptyProject = (workspaceID) => {
    return service.new(workspaceID)
  }


  return {
    state,
    fetchProjects,
    deleteProject,
    createProject,
    updateProject,
    newEmptyProject,
  }
}