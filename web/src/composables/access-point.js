import { ref, computed } from 'vue'
import {NewAccessPointRestRepository} from '@/repositories/access-point/rest.js'
import {NewAccessPointService} from '@/services/access-point.js'


/**
 * composable para workspaces
 * @returns Object
 */
export function useAccessPoint() {

  const state = ref([])
  const repo = NewAccessPointRestRepository({uri: '/api/workspaces'})
  const service = NewAccessPointService(repo)

  async function fetchAccessPoints(workspaceID, projectID) {
    try {
      const result = await service.getAll(workspaceID, projectID)
      state.value = result
    } catch(err) {
      console.error("composable:", err)
      throw(err)
    }
  }

  const updateAccessPoint = async (data) => {
    try {
      const response = await service.update(data)
      return response
    } catch(e) {
      console.error("error al actualizar proyecto", e)
      throw(e)
    }
  }

  const createAccessPoint = async ({workspaceID, projectID, name, type, lat, lng}) => {
    const ap = newEmptyAccessPoint(workspaceID, projectID)
    ap.name = name
    ap.type = type
    ap.lat = lat
    ap.lng = lng
    const result = await service.create(ap)
    if (state.value==null){
      state.value= []
    }
    state.value.push(...result)
    return result
  }

  const newEmptyAccessPoint = (workspaceID, projectID) => {
    return service.new(workspaceID, projectID)
  }


  const nodeLatLng = computed(()=>{
    if(state.value == null) {
      return {
        lat: -34,
        lng: -58
      }
    }
    const node =  state.value.find((v)=>{
      return v.type == 'node'
    })


    if(node == null) {
      return {
        lat: -34,
        lng: -58
      }
    }


    return {
      lat: node.lat,
      lng: node.lng
    }
  })

  const nodeList = computed(()=>{
    if(!state.value) {
      return []
    }

    return state.value.filter((ap) => {
       return ap.type == "node"
    })
  })

  const ceList = computed(()=>{
    if(!state.value) {
      return []
    }
    return state.value.filter((ap) => {
      return ap.type == "ce"
    })
  })

  const ctoList = computed(()=>{
    if(!state.value) {
      return []
    }
    return state.value.filter((ap) => {
      return ap.type == "cto"
    })
  })

  const customerList = computed(()=>{
    if(!state.value) {
      return []
    }
    return state.value.filter((ap) => {
      return ap.type == "customer"
    })
  })

  const poleList = computed(()=>{
    if(!state.value) {
      return []
    }
    return state.value.filter((ap) => {
      return ap.type == "pole"
    })
  })


  return {
    state,
    fetchAccessPoints,
    createAccessPoint,
    updateAccessPoint,
    nodeList, ceList, ctoList, customerList, poleList,
    nodeLatLng,
    newEmptyAccessPoint,
  }
}

