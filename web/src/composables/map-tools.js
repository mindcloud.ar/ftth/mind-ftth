
// const svgPointer = encodeURI(`<svg xmlns="http://www.w3.org/2000/svg" width='16' height='16' viewBox="0 0 1024 1024"><path fill="currentColor" d="M511.552 128c-35.584 0-64.384 28.8-64.384 64.448v516.48L274.048 570.88a94.272 94.272 0 0 0-112.896-3.456 44.416 44.416 0 0 0-8.96 62.208L332.8 870.4A64 64 0 0 0 384 896h512V575.232a64 64 0 0 0-45.632-61.312l-205.952-61.76A96 96 0 0 1 576 360.192V192.448C576 156.8 547.2 128 511.552 128M359.04 556.8l24.128 19.2V192.448a128.448 128.448 0 1 1 256.832 0v167.744a32 32 0 0 0 22.784 30.656l206.016 61.76A128 128 0 0 1 960 575.232V896a64 64 0 0 1-64 64H384a128 128 0 0 1-102.4-51.2L101.056 668.032A108.416 108.416 0 0 1 128 512.512a158.272 158.272 0 0 1 185.984 8.32z"></path></svg>`)
// const svgNode = encodeURI(`<svg xmlns="http://www.w3.org/2000/svg" width='16' height='16' viewBox="0 0 1024 1024"><path fill="currentColor" d="M192 128v704h384V128zm-32-64h448a32 32 0 0 1 32 32v768a32 32 0 0 1-32 32H160a32 32 0 0 1-32-32V96a32 32 0 0 1 32-32"></path><path fill="currentColor" d="M256 256h256v64H256zm0 192h256v64H256zm0 192h256v64H256zm384-128h128v64H640zm0 128h128v64H640zM64 832h896v64H64z"></path><path fill="currentColor" d="M640 384v448h192V384zm-32-64h256a32 32 0 0 1 32 32v512a32 32 0 0 1-32 32H608a32 32 0 0 1-32-32V352a32 32 0 0 1 32-32"></path></svg>`)
// const svgCE = encodeURI(`<svg xmlns="http://www.w3.org/2000/svg" width='16' height='16' viewBox="0 0 1024 1024"><path fill="currentColor" d="M800 416a288 288 0 1 0-576 0c0 118.144 94.528 272.128 288 456.576C705.472 688.128 800 534.144 800 416M512 960C277.312 746.688 160 565.312 160 416a352 352 0 0 1 704 0c0 149.312-117.312 330.688-352 544"></path><path fill="currentColor" d="M512 448a64 64 0 1 0 0-128 64 64 0 0 0 0 128m0 64a128 128 0 1 1 0-256 128 128 0 0 1 0 256m345.6 192L960 960H672v-64H352v64H64l102.4-256zm-68.928 0H235.328l-76.8 192h706.944z"></path></svg>`)
// const svgCTO = encodeURI(`<svg xmlns="http://www.w3.org/2000/svg" width='16' height='16' viewBox="0 0 1024 1024"><path fill="currentColor" d="M512 512a192 192 0 1 0 0-384 192 192 0 0 0 0 384m0 64a256 256 0 1 1 0-512 256 256 0 0 1 0 512"></path><path fill="currentColor" d="M512 512a32 32 0 0 1 32 32v256a32 32 0 1 1-64 0V544a32 32 0 0 1 32-32"></path><path fill="currentColor" d="M384 649.088v64.96C269.76 732.352 192 771.904 192 800c0 37.696 139.904 96 320 96s320-58.304 320-96c0-28.16-77.76-67.648-192-85.952v-64.96C789.12 671.04 896 730.368 896 800c0 88.32-171.904 160-384 160s-384-71.68-384-160c0-69.696 106.88-128.96 256-150.912"></path></svg>`)
// const svgCustomer = encodeURI(`<svg xmlns="http://www.w3.org/2000/svg" width='16' height='16' viewBox="0 0 1024 1024"><path fill="currentColor" d="M512 128 128 447.936V896h255.936V640H640v256h255.936V447.936z"></path></svg>`)

const defaultTool =   Object.freeze({
  type: "pointer",
  icon: "Pointer",
  pointer: `grab`,
  title: "Modo Puntero"
})

const tools = [
  defaultTool
  ,
  Object.freeze({
    type: "node",
    icon: "OfficeBuilding",
    pointer: `crosshair`,
    title: "Agregar Nodo"
  }),
  Object.freeze({
    type: "ce",
    icon: "MapLocation",
    pointer: `crosshair`,
    title: "Agregar Caja de Empalme"
  }),
  Object.freeze({
    type: "cto",
    icon: "Place",
    pointer: `crosshair`,
    title: "Agregar CTO"
  }),
  Object.freeze({
    type: "pole",
    icon: "Guide",
    pointer: `crosshair`,
    title: "Agregar Poste"
  }),
  Object.freeze({
    type: "customer",
    icon: "HomeFilled",
    pointer: `crosshair`,
    title: "Agregar Abonado"
  }),
]

import {computed} from 'vue'

export function useTools() {


  return {
    tools,
    defaultTool
  }
}
