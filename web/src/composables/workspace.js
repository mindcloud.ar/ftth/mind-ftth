import { ref, computed } from 'vue'
import {NewWorkspaceRestRepository} from '@repositories/workspace/rest.js'
import {NewWorkspaceService} from '@services/workspace.js'


/**
 * composable para workspaces
 * @returns Object
 */
export function useWorkspace() {

  const state = ref([])
  const repo = NewWorkspaceRestRepository({uri: '/api/workspaces'})
  const service = NewWorkspaceService(repo)

  async function fetchWorkspaces() {
    const result = await service.getAll()
    state.value = result
  }

  const getWorkspace = async (id) => {
    try {
      const response = await service.get(id)
      return response
    } catch(e) {
      console.error("error al obtener workspace", e)
      throw(e)
    }
  }

  const updateWorkspace = async (data) => {
    try {
      const response = await service.update(data)
      return response
    } catch(e) {
      console.error("error al actualizar workspace", e)
      throw(e)
    }
  }

  const createWorkspace = async (data) => {
    const result = await service.create(data)
    state.value.push(...result)
    return result
  }

    async function deleteWorkspace(id) {
    const response = await service.delete(id)

    // eliminar el elemento de la lista
    const idx = state.value.findIndex(el => el.id == id)
    if (idx >= 0) {
      state.value.splice(idx, 1)
    }

    return response
  }

  const workspacesActivos = computed(()=>{
    return state.value.filter((w)=>{
      return !w.archived
    })
  })

  const workspacesArchivados = computed(()=>{
    return state.value.filter((w)=>{
      return w.archived
    })
  })

  const newWorkspace = () => {
    return service.new()
  }

  return {
    state,
    fetchWorkspaces,
    getWorkspace,
    deleteWorkspace,
    createWorkspace,
    workspacesActivos,
    workspacesArchivados,
    newWorkspace,
    updateWorkspace,
  }
}