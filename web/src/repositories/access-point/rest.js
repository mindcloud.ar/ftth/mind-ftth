import {RestRepository} from '@mind-vue3-tools/repository'


class AccessPointRestRepository  extends RestRepository{
  constructor({uri}) {
    super({uri})
    this.template = {
      name: "",
      type: "",
      workspaceID: null,
      projectID: null,
      lat: null,
      lng: null,
    }
  }

  uri() {
    return `${this.baseUri}/${this.workspaceID}/projects/${this.projectID}/access-points`
  }

  async getAll({workspace, projectID, filters = [], limit = 1000, offset = 0, sort = []} = {filters: [], limit: 0, offset:0, sort: []}) {

    if(workspace == undefined) {
      throw "workspace no especificado"
    }
    if(projectID == undefined) {
      throw "proyecto no especificado"
    }
    this.workspaceID = workspace
    this.projectID = projectID

    return super.getAll({filters, limit, offset, offset, sort})

  }


  async create(data) {
    if(data.id) {
      throw "accion no permitida: el proyecto no pued contener el id"
    }

    if(!data.workspaceID) {
      throw "workspace no especificado"
    }

    if(!data.projectID) {
      throw "projectID no especificado"
    }

    this.workspaceID = data.workspaceID
    this.projectID = data.projectID

    return super.create(data)
  }

  async update(data) {
    if(data.id == undefined) {
      throw "id de Access Point no especificado"
    }
    if(!data.projectID) {
      throw "projectID no especificado"
    }

    if(data.workspaceID == undefined) {
      throw "workspace no especificado"
    }
    this.workspaceID = data.workspaceID
    this.projectID = data.projectID

    return super.update(data.id, data)
  }

}

export function NewAccessPointRestRepository(uri) {
  return new AccessPointRestRepository(uri)

}