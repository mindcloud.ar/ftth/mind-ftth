import {RestRepository} from '@mind-vue3-tools/repository'


class WorkspaceRestRepository  extends RestRepository{
  constructor({uri}) {
    super({uri})


    this.template = {
      name: "",
      description: "",
      zoom: 5,
      lat: null,
      lng: null,
    }
  }
}

export function NewWorkspaceRestRepository(uri) {
  return new WorkspaceRestRepository(uri)

}