import {RestRepository} from '@mind-vue3-tools/repository'


class ProjectRestRepository  extends RestRepository{
  constructor({uri}) {
    super({uri})
    this.template = {
      name: "",
      description: "",
      workspaceID: null,
      zoom: 5,
      lat: null,
      lng: null,
    }

  }
  uri() {
    return `${this.baseUri}/${this.workspaceID}/projects`
  }


  async getAll({workspace, filters = [], limit = 1000, offset = 0, sort = []} = {filters: [], limit: 0, offset:0, sort: []}) {

    if(workspace == undefined) {
      throw "workspace no especificado"
    }
    this.workspaceID = workspace

    try {
      const projects = await super.getAll({filters, limit, offset, offset, sort})
      return projects
    } catch(err) {
      console.err("repository", err)
      throw("no fue posible obtener la lista de proyectos")
    }


  }

  async get(workspace, id) {
    if(workspace == undefined) {
      throw "workspace no especificado"
    }
    this.workspaceID = workspace

    return super.get(id)
  }

  async delete(workspace, id) {
    if(workspace == undefined) {
      throw "workspace no especificado"
    }
    this.workspaceID = workspace

    return super.delete(id)
  }

  async create(data) {
    if(data.id) {
      throw "accion no permitida: el proyecto no pued contener el id"
    }

    if(!data.workspaceID) {
      throw "workspace no especificado"
    }
    this.workspaceID = data.workspaceID

    return super.create(data)
  }

  async update(data) {
    if(data.id == undefined) {
      throw "id de proyecto no especificado"
    }

    if(data.workspaceID == undefined) {
      throw "workspace no especificado"
    }
    this.workspaceID = data.workspaceID

    return super.update(data.id, data)
  }
}

export function NewProjectRestRepository(uri) {
  return new ProjectRestRepository(uri)

}