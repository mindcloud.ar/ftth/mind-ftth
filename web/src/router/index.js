import { createWebHistory, createRouter } from "vue-router";
import Home from "@/views/home.vue";
import Mapa from "@/components/HelloWorld.vue";
import Workspaces from "@/views/workspaces.vue";
import Workspace from "@/views/workspace.vue";

const routes = [
  {
    path: "/",
    name: "Inicio",
    component: Home,
		meta: {
			view: {
				title: 'Inicio',
			},
			nav: {
				icon: 'House',
			},
		}
  },

  {
    path: "/workspaces",
    name: "Espacios de Trabajo",
    component: Workspaces,
		meta: {
			view: {
				title: 'Espacios de Trabajo',
			},
			nav: {
				icon: 'Folder',
			},
		},
  },
  {
    path: '/workspaces/:id',
    name: 'Espacio de Trabajo',
    component: Workspace,
    meta: {
        view: {
            title: 'Espacio de Trabajo',
        },
    }
  },
// {
  //   path: "/mapa",
  //   name: "Prueba",
  //   component: Mapa,
  //   meta: {
	// 		nav: {
	// 			icon: 'Location',
	// 		},

  //   }
  // },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;