
class WorkspaceService {
  constructor(repository) {
    if(repository == undefined) {
      throw "se requiere un repositorio"
    }
    this.repository = repository
  }


  /**
   * obtiene la lista
   * @returns Promise
   */
  async getAll() {
      const result = await this.repository.getAll({})
      return result
  }

 async get(id) {
    let response
    try {
      response =  await this.repository.get(id)
    } catch(err) {
      console.error(err)
      throw(err)
    }
    return response
  }

  async update(data) {
    let response
    try {
      response =  await this.repository.update(data.id, data)
    } catch(err) {
      console.error(err)
      throw(err)
    }
    return response
  }

  /**
   * elimina un workspace
   * @param {*} id
   * @returns Promise
   */
  delete(id) {
    return new Promise((resolve, reject)=>{
      this.repository.delete(id).then((result)=>{
        resolve(result)
      }).catch((result)=>{
        reject(result)
      })

    })
  }

  /**
   * crea un workspace
   * @param {} workspace  objeto con los datos del workspace a crear
   * @returns
   */
  create(workspace) {
    return new Promise((resolve, reject)=>{
      this.repository.create(workspace).then((result)=>{
        resolve(result)
      }).catch((result)=>{
        reject(result)
      })
    })
  }

  /**
   *
   * @param {*} workspace objeto con los datos del workspace a salvar
   * @returns
   */
  save(workspace) {
    return new Promise((resolve, reject)=>{
      this.repository.save(workspace).then((result)=>{
        resolve(result)
      }).catch((result)=>{
        reject(result)
      })
    })
  }

  new() {
    return this.repository.new()
  }

}

/**
 * Devuelve un servicio de Workspace
 * @param {*} repository repositorio que implementa la comunicación con el backend
 * @returns
 */
export function NewWorkspaceService(repository) {
  return new WorkspaceService(repository)
}



