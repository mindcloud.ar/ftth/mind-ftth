
class ProjectService {
  constructor(repository) {
    if(repository == undefined) {
      throw "se requiere un repositorio"
    }
    this.repository = repository
  }


  /**
   * obtiene la lista
   * @returns Promise
   */
  async getAll(workspace) {
    try {
      const result = await this.repository.getAll({workspace}) || []
      return result
    } catch(err) {
      console.log("service", err)
      throw(err)
    }
  }

  async update(data) {
    try {
      const response =  await this.repository.update(data)
      return response
    } catch(err) {
      console.error(err)
      throw(err)
    }
  }


  /**
   * Elimina el project solicitado
   * @param {*} id id del project a eliminar
   * @returns
   */
  get(id) {
    return new Promise((resolve, reject)=>{
      this.repository.get(id).then((result)=>{
        resolve(result)
      }).catch((result)=>{
        reject(result)
      })

    })
  }



  /**
   * elimina un project
   * @param {*} id
   * @returns Promise
   */
  delete(id) {
    return new Promise((resolve, reject)=>{
      this.repository.delete(id).then((result)=>{
        resolve(result)
      }).catch((result)=>{
        reject(result)
      })

    })
  }

  /**
   * crea un project
   * @param {} project  objeto con los datos del project a crear
   * @returns
   */
  create(project) {
    return new Promise((resolve, reject)=>{
      this.repository.create(project).then((result)=>{
        resolve(result)
      }).catch((result)=>{
        reject(result)
      })
    })
  }

  /**
   *
   * @param {*} project objeto con los datos del project a salvar
   * @returns
   */
  save(project) {
    return new Promise((resolve, reject)=>{
      this.repository.save(project).then((result)=>{
        resolve(result)
      }).catch((result)=>{
        reject(result)
      })
    })
  }

  new(workspaceID) {
    if(!workspaceID) {
      throw("se requiere id de workspace")
    }
    const project = this.repository.new()
    project.workspaceID = workspaceID
    return project
  }


}

/**
 * Devuelve un servicio de Workspace
 * @param {*} repository repositorio que implementa la comunicación con el backend
 * @returns
 */
export function NewProjectService(repository) {
  return new ProjectService(repository)
}



