
class AccessPointService {
  constructor(repository) {
    if(repository == undefined) {
      throw "se requiere un repositorio"
    }
    this.repository = repository
  }


  /**
   * obtiene la lista
   * @returns Promise
   */
  async getAll(workspace, projectID) {
    try {
      const result = await this.repository.getAll({workspace, projectID})
      return result
    } catch(err) {
      console.error("service:", err)
      throw("error al obtener lista de puntos de acceso")
    }
  }


  /**
   * Elimina el accessPoint solicitado
   * @param {*} id id del accessPoint a eliminar
   * @returns
   */
  get(id) {
    return new Promise((resolve, reject)=>{
      this.repository.get(id).then((result)=>{
        resolve(result)
      }).catch((result)=>{
        reject(result)
      })

    })
  }

  async update(data) {
    try {
      const response =  await this.repository.update(data)
      return response
    } catch(err) {
      console.error(err)
      throw(err)
    }
  }



  /**
   * elimina un accessPoint
   * @param {*} id
   * @returns Promise
   */
  delete(id) {
    return new Promise((resolve, reject)=>{
      this.repository.delete(id).then((result)=>{
        resolve(result)
      }).catch((result)=>{
        reject(result)
      })

    })
  }

  create(data) {
    return new Promise((resolve, reject)=>{
      this.repository.create(data).then((result)=>{
        resolve(result)
      }).catch((result)=>{
        reject(result)
      })
    })
  }

  save(data) {
    return new Promise((resolve, reject)=>{
      this.repository.save(data).then((result)=>{
        resolve(result)
      }).catch((result)=>{
        reject(result)
      })
    })
  }

  new(workspaceID, projectID) {
    if(!workspaceID) {
      throw("se requiere id de workspace")
    }
    if(!projectID) {
      throw("se requiere id de proyecto")
    }
    const accessPoint = this.repository.new()
    accessPoint.workspaceID = workspaceID
    accessPoint.projectID = projectID
    return accessPoint
  }


}

/**
 * Devuelve un servicio de Workspace
 * @param {*} repository repositorio que implementa la comunicación con el backend
 * @returns
 */
export function NewAccessPointService(repository) {
  return new AccessPointService(repository)
}



