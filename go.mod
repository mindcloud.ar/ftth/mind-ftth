module gitlab.com/mindcloud.ar/ftth/mind-ftth

go 1.21.5

replace gitlab.com/mindcloud.ar/dev-tools/go-tools => ./submodule/go-tools

require (
	github.com/go-chi/chi v1.5.5
	github.com/go-sqlx/sqlx v1.3.6
	github.com/rs/zerolog v1.31.0
	gitlab.com/mindcloud.ar/dev-tools/go-tools v0.0.0-00010101000000-000000000000
)

require (
	github.com/ajg/form v1.5.1 // indirect
	github.com/distribution/reference v0.5.0 // indirect
	github.com/docker/docker v25.0.1+incompatible // indirect
	github.com/docker/go-connections v0.5.0 // indirect
	github.com/docker/go-units v0.5.0 // indirect
	github.com/go-chi/render v1.0.3 // indirect
	github.com/go-sql-driver/mysql v1.7.1 // indirect
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/golang-migrate/migrate v3.5.4+incompatible // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/opencontainers/image-spec v1.0.2 // indirect
	go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp v0.47.0 // indirect
	go.opentelemetry.io/otel/trace v1.22.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
)
