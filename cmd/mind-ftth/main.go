package main

import (
	// paquetes nativos
	"net/http"
	"os"

	// paquetes de terceros
	"github.com/go-chi/chi"
	// "github.com/go-sqlx/sqlx"
	_log "github.com/rs/zerolog/log"

	// helpers
	app "gitlab.com/mindcloud.ar/dev-tools/go-tools/app"
	// "gitlab.com/mindcloud.ar/dev-tools/go-tools/dbhelper"
	h "gitlab.com/mindcloud.ar/dev-tools/go-tools/handler"
	"gitlab.com/mindcloud.ar/dev-tools/go-tools/middleware"

	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/adapters/handlers"
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/ports"
	"gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/core/services"

	// repositories
	aps "gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/adapters/repository/accesspoint"
	apts "gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/adapters/repository/accesspointtype"
	ps "gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/adapters/repository/project"
	gs "gitlab.com/mindcloud.ar/ftth/mind-ftth/internal/adapters/repository/workspace"
)

var log = _log.With().Str("pkg", "main").Logger()

func main() {
	bindPort := os.Getenv("BIND_PORT")
	if bindPort == "" {
		bindPort = "8080"
	}

	// var err error

	//Inicialización de API Rest
	r := chi.NewRouter()

	// repositorios
	var (
		workspaceStore       ports.WorkspaceRepositorier
		projectStore         ports.ProjectRepositorier
		accessPointTypeStore ports.AccessPointTypeRepositorier
		accessPointStore     ports.AccessPointRepositorier
	)

	if os.Getenv("ENV") == "dev" {
		app.DevMode = true
		// migrationFolder = "default"
		log.Warn().Msg("Inciando en Modo Desarrollador")

		// repositorios de prueba
		workspaceStore = gs.NewWorkspaceMockRepository()
		projectStore = ps.NewProjectMockRepository()
		accessPointTypeStore = apts.NewAccessPointTypeMockRepository()
		accessPointStore = aps.NewAccessPointMockRepository()

	} else {
		// Inicialización de BD.
		/*
			    	var dbConn *sqlx.DB
				    migrationFolder := "default"

						dbHost := os.Getenv("DB_HOST")
						dbPort := os.Getenv("DB_PORT")
						dbName := os.Getenv("DB_SCHEMA")
						dbUser := os.Getenv("DB_USER")
						dbPass := os.Getenv("DB_PASSWORD")
						dbConn, err = dbhelper.NewConnection("mysql", dbHost, dbPort, dbName, dbUser, dbPass, migrationFolder)
						if err != nil {
							log.Fatal().Err(err).Str("cat", "db").Msg("No se pudo conectar a la BD")
							return
						}

						projectStore = ps.NewProjectMysqlRepository(dbConn)
		*/
	}

	// servicios
	var (
		groupService   = services.NewWorkspaceService(workspaceStore)
		projectService = services.NewProjectService(projectStore)
		aptService     = services.NewAccessPointTypeService(accessPointTypeStore)
		apService      = services.NewAccessPointService(accessPointStore, projectStore)
	)

	r.Get("/ping", h.Ping)

	r.Route("/api", func(r chi.Router) {
		r.Use(middleware.ParseParamsCtx)
		r.Get("/ping", h.Ping)
		r.Mount(`/workspaces`, handlers.NewGroupHandler(groupService))
		r.Mount(`/workspaces/{workspace-id}/projects`, handlers.NewProjectHandler(projectService))
		r.Mount(`/workspaces/{workspace-id}/projects/{project-id}/access-points`, handlers.NewAccessPointHandler(apService))
		r.Mount(`/config/access-point-type`, handlers.NewAccessPointTypeHandler(aptService))
	})

	log.Info().Str("port", bindPort).Msg("Iniciando Servicio")
	if err := http.ListenAndServe(":"+bindPort, r); err != nil {
		log.Fatal().Err(err).Str("cat", "http").Msg("No se pudo iniciar servicio http")
	}
}
