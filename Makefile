prepare:
	cd web && yarn install

build:
	go build ./cmd/mind-ftth

install:
	go install ./cmd/mind-ftth

# inicia suite de desarrollo
dev: prepare
	cd web && yarn dev


# dev-ps:
# 	cd dev && docker-compose ps

# logs:
# 	cd dev && docker-compose logs -f


# down:
# 	cd dev && docker-compose down --remove-orphans

# clean:
# 	cd dev && docker image prune -a -f

